package com.revature.expandablerecyclerview

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView
import com.example.revvaccine.R

class FAQAdapter internal constructor(private val context: Context, private val faqQuestions:List<String>, private val faqAnswers: HashMap<String, List<String>>): BaseExpandableListAdapter()
{
    override fun getGroupCount(): Int {
        return faqQuestions.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return this.faqAnswers[this.faqQuestions[groupPosition]]!!.size
    }

    override fun getGroup(groupPosition: Int): Any {
        return faqQuestions[groupPosition]
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return this.faqAnswers[this.faqQuestions[groupPosition]]!![childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(
        groupPosition: Int,
        isExpanded: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var convertView = convertView
        val chapterTitle = getGroup(groupPosition) as String

        if(convertView == null)
        {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.faq_questions, null)
        }

        val chapterTv = convertView!!.findViewById<TextView>(R.id.question_tv)
        chapterTv.setText(chapterTitle)

        return convertView
    }

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        var convertView = convertView
        val topicTitle = getChild(groupPosition, childPosition) as String

        if(convertView == null)
        {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater.inflate(R.layout.faq_answers, null)
        }

        val topicTv = convertView!!.findViewById<TextView>(R.id.answer_tv)
        topicTv?.setText(topicTitle)

        return convertView
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }
}