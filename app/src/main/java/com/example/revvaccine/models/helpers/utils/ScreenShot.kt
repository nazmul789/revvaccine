package com.example.revvaccine.models.helpers.utils

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object ScreenShot {
    private lateinit var timeStamp: String


    fun screenShot(activity: Activity, view: View) {

        try {
            timeStamp = SimpleDateFormat("yyyy-MM-dd_HH:mm").format(Date())
            val directory =
                File(activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES), "Certificate")

            if (!directory.exists())
                directory.mkdir()


            val pathname = "$directory/certificate-$timeStamp.png"
            view.isDrawingCacheEnabled = true
            val bitmap: Bitmap = Bitmap.createBitmap(view.drawingCache)
            view.isDrawingCacheEnabled = false

            val imageFile = File(pathname)
            val fileOutputStream = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.PNG, 90, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()

            shareScreenShot(activity,imageFile)
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    private fun shareScreenShot(activity: Activity, imageFile: File) {
        try {

            val uri: Uri = FileProvider.getUriForFile(
                activity.applicationContext,
                "com.example.revvaccine.fileprovider",
                imageFile
            )
            val intent = Intent().apply {
                type = "image/*"
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_SUBJECT, "Certificate created $timeStamp")
                putExtra(Intent.EXTRA_STREAM, uri)
            }

            activity.startActivity(Intent.createChooser(intent, "Send to"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}