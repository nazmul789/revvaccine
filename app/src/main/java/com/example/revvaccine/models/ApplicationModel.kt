package com.example.revvaccine.models

import com.example.revvaccine.ClickerApplication
import com.example.revvaccine.dataBase.BookingsTable.BookingsRepository
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecordsRepository
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocumentRepository
import com.example.revvaccine.server.Requests
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


object ApplicationModel {
    var firebase = FirebaseAuth.getInstance()

    fun isLoggedIn() = firebase.currentUser != null

    fun signout() {
        firebase.signOut()
        GlobalScope.launch {
            Requests.clearLocal()
        }
    }
}