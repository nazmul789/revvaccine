package com.example.revvaccine.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import com.example.revvaccine.dataBase.BookingsTable.BookingsRepository
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocument
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocumentRepository
import kotlinx.coroutines.launch

class VaccineRegistrationViewModel(private val repository: VaccineVerificationDocumentRepository) :
    ViewModel() {

    fun getAll(): LiveData<List<VaccineVerificationDocument>> = repository.getBookingsAll()
    fun getVerificationBySSN(ssn: String): LiveData<VaccineVerificationDocument> = repository.getVerificationBySSN(ssn)

    fun insert(vaccineVerificationDocument: VaccineVerificationDocument) = viewModelScope.launch {
        repository.insert(vaccineVerificationDocument)
    }

    fun deleteBySsn(refId: String) = viewModelScope.launch {
        repository.deleteBySsn(refId)
    }

    fun deleteAll() = viewModelScope.launch {
        repository.deleteAll()
    }

}




class VaccineRegistrationFactory(private val repository: VaccineVerificationDocumentRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VaccineRegistrationViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return VaccineRegistrationViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
