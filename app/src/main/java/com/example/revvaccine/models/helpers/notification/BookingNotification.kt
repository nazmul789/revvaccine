package com.example.revvaccine.models.helpers.notification

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import androidx.core.app.NotificationCompat
import com.example.revvaccine.MainActivity
import com.example.revvaccine.R

private val NOTIFICATION_ID = 0
val GROUP_KEY_WORK_BOOKINGS = "com.android.example.BOOKINGS"

fun NotificationManager.sendBookingNotification(
    applicationContext: Context,
    title: String,
    messageBody: String,
    setWhen: Long,
    isCanceledOrDenied: Boolean = false
) {

    val intent = Intent(applicationContext, MainActivity::class.java)
    val contentPendingIntent = PendingIntent.getActivity(
        applicationContext,
        NOTIFICATION_ID,
        intent,
        PendingIntent.FLAG_CANCEL_CURRENT
    )

    val imgId = if (isCanceledOrDenied) R.drawable.injection_denied else R.drawable.injection_approved

    var builder = NotificationCompat.Builder(
        applicationContext,
        applicationContext.getString(R.string.booking_notification_channel_id)
    )
        .setSmallIcon(imgId)
        .setContentTitle(title)
        .setContentText(messageBody)
        .setDefaults(Notification.DEFAULT_SOUND and Notification.DEFAULT_VIBRATE)
        .setContentIntent(contentPendingIntent)
        .setSmallIcon(imgId)
        .setWhen(setWhen)
        .setLargeIcon(
            BitmapFactory.decodeResource(
                applicationContext.resources,
                imgId
            )
        )
        .setPriority(NotificationCompat.PRIORITY_MAX)
        .setAutoCancel(true)
        .setGroup(GROUP_KEY_WORK_BOOKINGS)

    notify(NOTIFICATION_ID, builder.build())
}


fun NotificationManager.cancelNotifications() {
    cancelAll()
}
