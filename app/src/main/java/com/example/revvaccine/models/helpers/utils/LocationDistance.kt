package com.example.revvaccine.models.helpers.utils

import android.location.Location


fun LocationDistance(
    lat1: Double,
    long1: Double,
    lat2: Double,
    long2: Double,
): Double {
    val loc1 = Location("")
    loc1.latitude = lat1
    loc1.longitude = long1
    val loc2 = Location("")
    loc2.latitude = lat2
    loc2.longitude = long2
    return loc1.distanceTo(loc2 ) / 1609.344
}


