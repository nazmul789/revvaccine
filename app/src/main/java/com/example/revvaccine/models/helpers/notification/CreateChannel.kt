package com.example.revvaccine.models.helpers.notification

import android.app.Activity
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.graphics.Color
import android.os.Build

fun createNotificationChannel(activity: Activity, channelId: String, channelName: String) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
        val channel = NotificationChannel(
            channelId,
            channelName,
            NotificationManager.IMPORTANCE_DEFAULT
        ).apply {
            description = "description Text"
            enableVibration(true)
            enableLights(true)
            lightColor= Color.RED
            importance= NotificationManager.IMPORTANCE_HIGH
        }.apply {
            setShowBadge(false)
        }
        // Register the channel with the system
        val notificationManager: NotificationManager =
            activity.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }
}