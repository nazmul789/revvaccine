package com.example.revvaccine.models.helpers.locationSearch

data class Location(
    val locationID: String,
    val url: String,
    val city: String,
    val name: String,
    val state: String,
    val address: String,
    val postalCode: String,
    val carriesVaccine: String,
    val appointmentsAvailable: String,
    val appointmentsLastFetched: String,
    val appointmentsAvailableAllDoses: String,
    val appointmentsAvailableSecondDoseOnly: String,
    val pfizerAvailable: Boolean,
    val unknownAvailable: Boolean,
    val modernaAvailable: Boolean,
    val jnjAvaiable: Boolean,
    val latitude: Double,
    val longitude: Double,
)