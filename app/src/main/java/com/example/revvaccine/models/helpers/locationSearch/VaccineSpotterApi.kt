package com.example.revvaccine.models.helpers.locationSearch

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.volley.Request
import com.android.volley.toolbox.JsonObjectRequest
import org.json.JSONObject
import timber.log.Timber
import java.sql.Time
import java.util.*


class VaccineSpotterApi {
    val locationsList: MutableLiveData<ArrayList<Location>> = MutableLiveData()
    val error: MutableLiveData<Boolean> = MutableLiveData(false)

    fun getLocations(): LiveData<ArrayList<Location>> = locationsList

    fun call(
        context: Context,
        state: String,
        postalCode: String,
        radius: Int = 10,
    ) {

        val baseUrl = "https://www.vaccinespotter.org/api/v0/states/$state.json?zip=$postalCode&radius=$radius"
        val locationList = ArrayList<Location>()


        val jsonObjectRequest = JsonObjectRequest(
            Request.Method.GET, baseUrl, null,
            { response ->
                val jsonArr = response.getJSONArray("features")
                Timber.i(jsonArr.length().toString())

                for (i in 0 until jsonArr.length()) {
                    val loc = jsonArr.getJSONObject(i)
                    val extract = extractData(loc)
                    if(extract != null) locationList.add(extract)
                }
                Timber.i("VaccineSpotterApi result size ${locationList.size}")
                locationsList.postValue(locationList)
            },
            { e ->
                Timber.i(e.printStackTrace().toString())
                error.postValue(true)
            }
        )

        return VolleyRequest.getInstance(context).addToRequestQueue(jsonObjectRequest)
    }


    private fun extractData(loc: JSONObject): Location? {

        /** Geometry*/
        val geometry = loc.getJSONObject("geometry")
        val coord = geometry.getJSONArray("coordinates") // good,

        if(coord[1].toString().trim() == "null" || coord[0].toString().trim() == "null" ) return null

        val latitude = coord[1].toString().toDouble()//good
        val longitude = coord[0].toString().toDouble()//Goodd

        /** Properties */
        val properties = loc.getJSONObject("properties")
        val locationID = properties.getString("id")
        val name = properties.getString("name")
        val address = properties.getString("address")
        val city = properties.getString("city")
        val state = properties.getString("state")
        val postalCode = properties.getString("postal_code")
        val url = properties.getString("url")
        val carriesVaccine = properties.getString("carries_vaccine")
        val appointmentsAvailable = properties.getString("appointments_available")
        val appointmentsLastFetched = properties.getString("appointments_last_fetched")
        val appointmentsAvailableAllDoses =
            properties.getString("appointments_available_all_doses")
        val appointmentsAvailableSecondDoseOnly =
            properties.getString("appointments_available_2nd_dose_only")

        val vaccineType =
            if (properties.isNull("appointment_vaccine_types")) null else properties.getJSONObject(
                "appointment_vaccine_types"
            )
        var pfizerAvailable: Boolean = false
        var unknownAvailable: Boolean = false
        var modernaAvailable: Boolean = false
        var jnjAvaiable: Boolean = false


        if (vaccineType != null) {
            pfizerAvailable =
                if (vaccineType.isNull("pfizer")) false else vaccineType.getBoolean("pfizer")
            unknownAvailable =
                if (vaccineType.isNull("unknown")) false else vaccineType.getBoolean("unknown")
            modernaAvailable =
                if (vaccineType.isNull("moderna")) false else vaccineType.getBoolean("moderna")
            jnjAvaiable =
                if (vaccineType.isNull("jj")) false else vaccineType.getBoolean("jj")
        }

        return Location(
            locationID = locationID,
            url = url,
            city = city,
            name = name,
            state = state,
            address = address,
            postalCode = postalCode,
            carriesVaccine = carriesVaccine,
            appointmentsAvailable = appointmentsAvailable,
            appointmentsLastFetched = appointmentsLastFetched,
            appointmentsAvailableAllDoses = appointmentsAvailableAllDoses,
            appointmentsAvailableSecondDoseOnly = appointmentsAvailableSecondDoseOnly,
            pfizerAvailable = pfizerAvailable,
            unknownAvailable = unknownAvailable,
            modernaAvailable = modernaAvailable,
            jnjAvaiable = jnjAvaiable,
            latitude = latitude,
            longitude = longitude,
        )
    }

}
