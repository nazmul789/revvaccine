package com.example.revvaccine.models.helpers.utils

import android.content.Context
import android.widget.Toast
import androidx.navigation.NavController
import com.example.revvaccine.models.ApplicationModel

fun navigateToHome(context: Context, navController: NavController, navTarget: Int) {
    if (!ApplicationModel.isLoggedIn()) {
        navController.navigate(navTarget)
        Toast.makeText(context, "Login required, please login.", Toast.LENGTH_SHORT).show()
    }
}