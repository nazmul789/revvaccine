package com.example.revvaccine.models.helpers.bookingList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import java.util.*

class BookingDataViewModel : ViewModel() {
     var fistName = "Jane"
     var lastName = "Doe"
     var refID = "ABCDEA"
     var ssn = "123456789"
     var appointmentDate = "2021-05-22"
     var appointmentTime = "03:22 AM"
    var bookings: Bookings? = null
    val calendar: Calendar = Calendar.getInstance()
}