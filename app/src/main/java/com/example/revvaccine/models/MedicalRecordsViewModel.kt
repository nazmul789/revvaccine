package com.example.revvaccine.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecordsRepository
import kotlinx.coroutines.launch

class MedicalRecordsViewModel(
    private val repository: MedicalRecordsRepository
) : ViewModel() {
    val allRecords: LiveData<List<MedicalRecords>> = repository.allRecords
    fun getRecordsBySSN(ssn: String): LiveData<MedicalRecords> = repository.getRecordsBySSN(ssn)

    fun insert(records: MedicalRecords) = viewModelScope.launch {
        repository.insert(records)
    }

}


class MedicalRecordsModelFactory(private val repository: MedicalRecordsRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MedicalRecordsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return MedicalRecordsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
