package com.example.revvaccine.models.helpers.bookingList

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.CalendarContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.example.revvaccine.R
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import com.example.revvaccine.models.ApplicationModel
import kotlinx.coroutines.yield
import java.util.*

class BookingAdapter(
    private val context: Context,
    private val navController: NavController,
    var bookings: List<Bookings>,
    private val bookingDataViewModel: BookingDataViewModel
) :
    RecyclerView.Adapter<BookingAdapter.ItemViewHolder>() {

    class ItemViewHolder(private val itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val fullName: TextView = itemView.findViewById(R.id.booking_full_name)
        val dateTime: TextView = itemView.findViewById(R.id.booking_date_time)
        val locationName: TextView = itemView.findViewById(R.id.booking_location_name)
        val locationAddress: TextView = itemView.findViewById(R.id.booking_location_address)
        val referenceID: TextView = itemView.findViewById(R.id.booking_reference)
        val openGoogleButton: Button = itemView.findViewById(R.id.booking_open_in_google_btn)
        val editButton: ImageButton = itemView.findViewById(R.id.booking_full_edit_button)
        val calendarButton: ImageButton = itemView.findViewById(R.id.booking_add_to_calendar)


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.fragment_booking_list_item,
            parent, false
        )

        return ItemViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = bookings[position]

        val address =
            "${item.locationAddress}, ${item.locationCity}, ${item.locationState} ${item.locationPostal}"

        holder.fullName.text = "${item.firstName}  ${item.lastName}"
        holder.dateTime.text = "${item.date}   ${item.time}"
        holder.locationName.text = item.locationName
        holder.locationAddress.text = address
        holder.referenceID.text = "Reference ID: ${item.referenceId}"


        holder.openGoogleButton.setOnClickListener {
            val gmmIntentUri =
                Uri.parse("geo:${item.locationLatitude},${item.locationLatitude}?q=" + Uri.encode(address))
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(context, mapIntent, null)

        }
        holder.editButton.setOnClickListener {
            bookingDataViewModel.fistName = item.firstName
            bookingDataViewModel.lastName = item.lastName
            bookingDataViewModel.refID = item.referenceId
            bookingDataViewModel.ssn = item.ssn
            bookingDataViewModel.bookings = item
            navController.navigate(R.id.bookingCalendar)
        }
        holder.calendarButton.setOnClickListener {
            val startMillis: Long = bookingDataViewModel.calendar.timeInMillis
            val endMillis = bookingDataViewModel.calendar.timeInMillis + 3.6e+6

            val intent = Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, startMillis)
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endMillis)
                .putExtra(CalendarContract.Events.TITLE, "Covid-19 Vaccine Appointment")
                .putExtra(
                    CalendarContract.Events.DESCRIPTION,
                    "Vaccine appointment at ${item.locationName}, located at $address  (${item.locationLatitude}, ${item.locationLongitude})"
                )
                .putExtra(
                    CalendarContract.Events.EVENT_LOCATION,
                    "$address  (${item.locationLatitude}, ${item.locationLongitude})"
                )
                .putExtra(
                    CalendarContract.Events.AVAILABILITY,
                    CalendarContract.Events.AVAILABILITY_BUSY
                )
                .putExtra(
                    Intent.EXTRA_EMAIL,
                    ApplicationModel.firebase.currentUser.email.toString()
                )
            context.startActivity(intent)

        }


    }

    override fun getItemCount(): Int = bookings.size
}
