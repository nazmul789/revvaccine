package com.example.revvaccine.models.helpers.locationSearch

import android.annotation.SuppressLint
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.*
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.recyclerview.widget.RecyclerView
import com.example.revvaccine.R
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.models.LocationSearchViewModel
import com.example.revvaccine.models.helpers.notification.sendBookingNotification
import com.example.revvaccine.models.helpers.utils.LocationDistance
import timber.log.Timber
import java.math.BigDecimal
import java.math.RoundingMode
import java.sql.Time
import java.util.ArrayList


/**
 * Resources: https://developers.google.com/maps/documentation/urls/android-intents#kotlin_1
 */

class LocationSearchResultListAdapter(
    private val context: Context,
    private val dataSet: List<Location>,
    private val viewModel: LocationSearchViewModel,
    private val navController: NavController
) : RecyclerView.Adapter<LocationSearchResultListAdapter.ItemViewHolder>() {

    @SuppressLint("ClickableViewAccessibility")
    class ItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        //        val textview: TextView = view.findViewById(R.id.location_search_list_item)
        val locationName: TextView = view.findViewById(R.id.location_search_location_name)
        val locationAddress: TextView = view.findViewById(R.id.location_search_location_address)
        val appointmentAvailable: TextView =
            view.findViewById(R.id.location_search_location_appointment_available)
        val distanctToLocation: TextView = view.findViewById(R.id.location_search_location_distance)

        val vaccineJnJ: CheckBox = view.findViewById(R.id.location_search_check_vaccine_jnj)
        val vaccineModerna: CheckBox = view.findViewById(R.id.location_search_check_vaccine_moderna)
        val vaccinePfizer: CheckBox = view.findViewById(R.id.location_search_check_vaccine_pfizer)
        val vaccineUnknown: CheckBox = view.findViewById(R.id.location_search_check_vaccine_unknown)
        val openInGoogleButton: Button = view.findViewById(R.id.location_search_button_open_google)
        val bookNowButton: Button = view.findViewById(R.id.location_search_button_book_now)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        //Now holds a ref to list item, inflates list item layout
        val adapterLayout = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.fragment_location_search_list_item, parent, false)
        return ItemViewHolder(adapterLayout)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val item = dataSet[position]
        val userLat = viewModel.getLat().value
        val userLong = viewModel.getLong().value

        val dist = if (userLat != null && userLong != null) {
            val calc = LocationDistance(
                userLat,
                userLong,
                item.latitude.toDouble(),
                item.longitude.toDouble()
            )
            "${BigDecimal(calc).setScale(2, RoundingMode.HALF_EVEN).toString()} miles"
        }
         else "Distance N/A, search using your location"


        var apptAvailMsg =
            if (item.appointmentsAvailable.toBoolean()) "Appointment available" else "No Appointment"
        apptAvailMsg =
            "$apptAvailMsg / ${if (item.appointmentsAvailableAllDoses.toBoolean()) "All Dose / " else ""}"
        apptAvailMsg =
            "$apptAvailMsg ${if (item.appointmentsAvailableSecondDoseOnly.toBoolean()) "Second Dose available" else " / Limited Availability"} "

        val addresses = if(
            item.address == "null" ||
            item.state =="null" ||
            item.postalCode=="null" ||
            item.city =="null"
        ) "${item.latitude}, ${item.longitude}"
        else
            "${item.address}, ${item.city}, ${item.state} ${item.postalCode} (${item.latitude}, ${item.longitude})"

        holder.locationName.text = item.name
        holder.locationAddress.text = addresses
        holder.appointmentAvailable.text = apptAvailMsg
        holder.distanctToLocation.text = dist
        holder.vaccineJnJ.isChecked = item.jnjAvaiable
        holder.vaccinePfizer.isChecked = item.pfizerAvailable
        holder.vaccineModerna.isChecked = item.modernaAvailable
        holder.vaccineUnknown.isChecked = item.unknownAvailable


        holder.openInGoogleButton.setOnClickListener {
            val gmmIntentUri =
                Uri.parse("geo:${item.latitude},${item.longitude}?q=" + Uri.encode(addresses))
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")
            startActivity(context, mapIntent, null)

        }

        holder.bookNowButton.isVisible = ApplicationModel.isLoggedIn()
        holder.bookNowButton.setOnClickListener {
            viewModel.setSelectedLocation(item)
            navController.navigate(R.id.action_locationSearchResultFragment_to_bookingCalendar)


        }


    }

    override fun getItemCount(): Int = dataSet.size

}