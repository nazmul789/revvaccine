package camera

import android.Manifest
import android.content.pm.PackageManager
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.example.revvaccine.R
import com.example.revvaccine.databinding.VaccinationRegistrationFragmentBinding
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber


/**
 * Resource: https://developer.android.com/training/camera/photobasics
 */
class CameraManager(val activity: AppCompatActivity, val view: View) :
    ActivityCompat.OnRequestPermissionsResultCallback {
    private val CAMERA_PERMISSION = Manifest.permission.CAMERA
    private val PERMISSION_REQUEST_CAMERA = 0
    private var hasPermission: Boolean = false

    fun getPermission() {
        if (checkPermission()) {
            hasPermission = true
            Snackbar.make(view, "Camera is access detected.", Snackbar.LENGTH_SHORT).show()
        } else requestPermission()
    }


    fun permissionAndImageStarter(
        callback: () -> Unit
    ): Boolean {
        getPermission()
        if (hasPermission) {
            callback()
            return true
        }

        return false
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            hasPermission = true
            Snackbar.make(view, "Camera is access granted.", Snackbar.LENGTH_SHORT).show()
        } else {
            Snackbar.make(view, "Camera is access denied.", Snackbar.LENGTH_SHORT).show()
            hasPermission = false
        }
    }


    fun checkPermission(): Boolean =
        ActivityCompat.checkSelfPermission(activity.applicationContext, CAMERA_PERMISSION) ==
                PackageManager.PERMISSION_GRANTED


    fun requestPermission() {
        //Snackbar for permission
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, CAMERA_PERMISSION)) {
            Snackbar.make(view, "Camera is required for photo proof.", Snackbar.LENGTH_INDEFINITE)
                .setAction("Allow") {
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(CAMERA_PERMISSION),
                        PERMISSION_REQUEST_CAMERA
                    )
                }.show()
        }
        //Request permission, sends result to onRequestPermission
        else {
            Snackbar.make(view, "Camera is required for photo proof.", Snackbar.LENGTH_SHORT)
                .setAction("Allow") {
                    ActivityCompat.requestPermissions(
                        activity,
                        arrayOf(CAMERA_PERMISSION),
                        PERMISSION_REQUEST_CAMERA
                    )
                }.show()
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(CAMERA_PERMISSION),
                PERMISSION_REQUEST_CAMERA
            )
        }

    }


}

