package com.example.revvaccine.models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import com.example.revvaccine.dataBase.BookingsTable.BookingsRepository
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecordsRepository
import kotlinx.coroutines.launch

class BookingsViewModel(
    private val repository: BookingsRepository
) : ViewModel() {
    fun getBookingsAll(): LiveData<List<Bookings>> = repository.getBookingsAll()
    fun getBookingsBySSN(ssn: String): LiveData<Bookings> = repository.getBookingsBySSN(ssn)
    fun getBookingsByReference(refId: String): LiveData<Bookings> =
        repository.getBookingsByReference(refId)


    fun insert(bookings: Bookings) = viewModelScope.launch {
        repository.insert(bookings)
    }

    fun deleteByRefId(refId: String) = viewModelScope.launch {
        repository.deleteByRef(refId)
    }



}



class BookingsModelFactory(private val repository: BookingsRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(BookingsViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return BookingsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
