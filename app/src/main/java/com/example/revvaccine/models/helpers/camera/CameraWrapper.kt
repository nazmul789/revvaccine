package camera

import android.content.ActivityNotFoundException
import android.content.Intent
import android.media.MediaScannerConnection
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import java.io.File
import android.os.Environment
import androidx.fragment.app.FragmentActivity
import com.example.revvaccine.databinding.VaccinationRegistrationFragmentBinding
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Resource: https://developer.android.com/training/camera/photobasics
 */
class CameraWrapper(private val activity: FragmentActivity) {
    var currentPhotoPath: String = ""
        private set
    private val REQUEST_IMAGE_CAPTURE = 1

    fun startPictureEvent(): Boolean {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(activity.packageManager).also {
                var photoFile: File = createImageFile()

                photoFile.also {
                    val photoURI: Uri? = FileProvider.getUriForFile(
                        activity.applicationContext,
                        "com.example.revvaccine.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                }

            }
        }
        try {
            activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
      return true
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            currentPhotoPath = absolutePath
        }

        galleryAddPic()
    }


    private fun galleryAddPic() =
        MediaScannerConnection
            .scanFile(
                activity.applicationContext,
                arrayOf(File(currentPhotoPath).toString()),
                null,
                null
            )


}
