package com.example.revvaccine.models

import android.view.View
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.revvaccine.models.helpers.locationSearch.Location
import java.lang.Exception

class LocationSearchViewModel : ViewModel() {
    private val zipCode = MutableLiveData<String>()
    private val state = MutableLiveData<String>()
     val latitude = MutableLiveData<Double>(null)

     val longitude = MutableLiveData<Double>(null)
    private val radius = MutableLiveData<Int>(20)
    private val selectedLocation = MutableLiveData<Location>()
    private var allLocation: List<Location>? = null



    fun getZipCode(): LiveData<String> = zipCode
    fun getState(): LiveData<String> = state
    fun getLat(): LiveData<Double> = latitude
    fun getLong(): LiveData<Double> = longitude
    fun getRadius(): LiveData<Int> = radius

    //for google maps
    fun getALlLocations(): List<Location>? = allLocation
    fun setALlLocations(value: List<Location>) {
        allLocation = value
    }

    //for bookings
    fun getSelectedLocation(): LiveData<Location> = selectedLocation
    fun setSelectedLocation(value: Location) = selectedLocation.postValue(value) //For booking


    //for location forms
    fun setZipCode(value: String) = zipCode.postValue(value)
    fun setState(value: String) = state.postValue(value)
    fun setLat(value: Double) = latitude.postValue(value)
    fun setLong(value: Double) = longitude.postValue(value)
    fun setRadius(value: Int) = radius.postValue(value)



}