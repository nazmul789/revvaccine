package com.example.revvaccine

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
//import com.android.volley.toolbox.*
import com.example.revvaccine.databinding.ActivityMainBinding
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.models.helpers.notification.createNotificationChannel
import com.example.revvaccine.models.helpers.utils.navigateToHome
import kotlinx.android.synthetic.main.fragment_home.*
import com.example.revvaccine.server.Requests

class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding =
            DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main)


         Requests.init(applicationContext as ClickerApplication)


        /**
         * Create channels
         */
        createNotificationChannel(
            this,
            getString(R.string.booking_notification_channel_id),
            getString(R.string.booking_notification_channel_Name)
        )

        /**
         * Set ups navigation
         */
        drawerLayout = binding.drawerLayout


        val navController = this.findNavController(R.id.NavHostFragment)


        val appConfig = AppBarConfiguration.Builder(setOf(R.id.nav_home, R.id.locationView))
            .setDrawerLayout(drawerLayout)
            .build()

        NavigationUI.setupActionBarWithNavController(this, navController, appConfig)
        NavigationUI.setupWithNavController(binding.navView, navController)

        // Logout function for nav bar
        binding.navView.menu.findItem(R.id.nav_logOut).setOnMenuItemClickListener { menuItem ->
            drawerLayout.closeDrawer(GravityCompat.START)
            ApplicationModel.signout()
            menuItem.isVisible = ApplicationModel.isLoggedIn()
            navController.navigate(R.id.nav_home)
            true
        }

        // binding for logout menu option
        binding.navView.menu.findItem(R.id.nav_logOut).isVisible = ApplicationModel.isLoggedIn()

        // binding for bookings menu option
        binding.navView.menu.findItem(R.id.nav_vaccine_bookings).setOnMenuItemClickListener {
            navigateToHome(this, navController, R.id.nav_home)
            navController.navigate(R.id.bookingsListFragment)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        // binding for view certification menu option
        binding.navView.menu.findItem(R.id.nav_vaccine_certification).setOnMenuItemClickListener {
            navigateToHome(this, navController, R.id.nav_home)
            navController.navigate(R.id.vaccineCertification)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        // binding for medical info menu option
        binding.navView.menu.findItem(R.id.nav_Medical_information).setOnMenuItemClickListener {
            navigateToHome(this, navController, R.id.nav_home)
            navController.navigate(R.id.userMedicalInformation)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        // binding for vaccine register menu option
        binding.navView.menu.findItem(R.id.nav_vaccine_register).setOnMenuItemClickListener {
            navigateToHome(this, navController, R.id.nav_home)
            navController.navigate(R.id.vaccinationRegistration)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        // binding for FAW menu option
        binding.navView.menu.findItem(R.id.nav_FAQ).setOnMenuItemClickListener {
            navController.navigate(R.id.FAQ)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        // binding for Resources menu option
        binding.navView.menu.findItem(R.id.nav_resources).setOnMenuItemClickListener {
            navController.navigate(R.id.resources)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        // binding for Contact Us menu option
        binding.navView.menu.findItem(R.id.nav_submitFeedback).setOnMenuItemClickListener {
            navController.navigate(R.id.submitFeedback)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        // binding for About Us menu option
        binding.navView.menu.findItem(R.id.nav_privacyPolicy).setOnMenuItemClickListener {
            navController.navigate(R.id.privacyPolicy)
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }

        //bottom navigation stuff
        NavigationUI.setupWithNavController(binding.bottomNavigation, navController)

        binding.bottomNavigation.menu.findItem(R.id.nav_home).setOnMenuItemClickListener {
            navController.navigate(R.id.nav_home)
            true
        }

        binding.bottomNavigation.menu.findItem(R.id.bookingsListFragment).setOnMenuItemClickListener {
            navController.navigate(R.id.bookingsListFragment)
            true
        }

        binding.bottomNavigation.menu.findItem(R.id.FAQ).setOnMenuItemClickListener {
            navController.navigate(R.id.FAQ)
            true
        }

        binding.bottomNavigation.menu.findItem(R.id.vaccineCertification).setOnMenuItemClickListener {
            navController.navigate(R.id.vaccineCertification)
            true
        }

    }

    // Function for changing views for the navigation drawer
    override fun onSupportNavigateUp(): Boolean {

        val appConfig = AppBarConfiguration.Builder(setOf(R.id.nav_home, R.id.locationView))
            .setDrawerLayout(drawerLayout)
            .build()
        val navController = this.findNavController(R.id.NavHostFragment)
        return NavigationUI.navigateUp(navController, appConfig)
    }




}