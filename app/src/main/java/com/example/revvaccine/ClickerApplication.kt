package com.example.revvaccine

import android.app.Application
import com.example.revvaccine.dataBase.ApplicationDataBase
import com.example.revvaccine.dataBase.BookingsTable.BookingsRepository
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecordsRepository
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocumentRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import timber.log.Timber
import java.sql.Time


class ClickerApplication : Application() {

    val applicationScope = CoroutineScope(SupervisorJob())

    val database by lazy { ApplicationDataBase.getDatabase(this, applicationScope) }
    val recordsRepository by lazy { MedicalRecordsRepository(database.medicalRecordsDao()) }
    val bookingsRepository by lazy { BookingsRepository(database.bookingsDao()) }
    val vaccineRegistrationRepository by lazy { VaccineVerificationDocumentRepository(database.vaccineVerificationDao()) }



    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }


        Thread.setDefaultUncaughtExceptionHandler(UncaughtExceptions())


    }


}

class UncaughtExceptions : Thread.UncaughtExceptionHandler {
    private var defaultHandler = Thread.getDefaultUncaughtExceptionHandler()
    override fun uncaughtException(t: Thread, e: Throwable) {
        if(BuildConfig.DEBUG){
            Timber.e("UNCAUGHT EXCEPTION")
            Timber.e(e.printStackTrace().toString())
        }
        defaultHandler?.uncaughtException(t,e)
    }
}

