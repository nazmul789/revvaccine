package com.example.revvaccine.view.navbar

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ExpandableListView
import com.example.revvaccine.R
import com.revature.expandablerecyclerview.FAQAdapter

class FAQ : Fragment() {
    private lateinit var listViewAdapter: FAQAdapter
    private lateinit var questionList: List<String>
    private lateinit var answerList: HashMap<String, List<String>>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        val view = inflater.inflate(R.layout.fragment_faq, container, false)

        showList()
        listViewAdapter = FAQAdapter(requireContext(), questionList, answerList)
        (view.findViewById<ExpandableListView>(R.id.faqEListView)).setAdapter(listViewAdapter)

        return view
    }

    private fun showList()
    {
        questionList = ArrayList()
        answerList = HashMap()

        (questionList as ArrayList<String>).add("How does the virus spread?")
        (questionList as ArrayList<String>).add("Can I choose which COVID vaccine I get?")
        (questionList as ArrayList<String>).add("What are the most common side effects after getting a COVID vaccine?")
        (questionList as ArrayList<String>).add("If I am pregnant, can I get a COVID vaccine?")
        (questionList as ArrayList<String>).add("How long does the protection last?")
        (questionList as ArrayList<String>).add("How many doses of COVID vaccine will I need to get?")
        (questionList as ArrayList<String>).add("If I have an underlying condition, can I get a COVID vaccine?")

        val answer1: MutableList<String> = ArrayList()
        answer1.add("COVID is thought to spread mainly through close contact from person to person, including between people who are physically near each other (within about 6 feet). COVID spreads very easily from person to person. The virus that causes COVID appears to spread more efficiently than influenza but not as efficiently as measles, which is among the most contagious viruses known to affect people.")

        val answer2: MutableList<String> = ArrayList()
        answer2.add("You should get any COVID vaccine that is available when you are eligible. Do not wait for a specific brand. All currently authorized and recommended COVID-19 vaccines are safe and effective, and CDC does not recommend one vaccine over another.")

        val answer3: MutableList<String> = ArrayList()
        answer3.add("After getting vaccinated, you might have some side effects, which are normal signs that your body is building protection. Common side effects are pain, redness, and swelling in the arm where you received the shot, as well as tiredness, headache, muscle pain, chills, fever, and nausea throughout the rest of the body. The symptoms should go away in a few days")

        val answer4: MutableList<String> = ArrayList()
        answer4.add("Yes you can. However, there are currently limited data on the safety of COVID vaccines in pregnant people because these vaccines have not been widely studied in pregnant people.")

        val answer5: MutableList<String> = ArrayList()
        answer5.add("It is currently unknown how long protection lasts for those who are vaccinated.")

        val answer6: MutableList<String> = ArrayList()
        answer6.add("The number of doses needed depends on which vaccine you receive")

        val answer7: MutableList<String> = ArrayList()
        answer7.add("People with underlying medical conditions can receive a COVID-19 vaccine as long as they have not had an immediate or severe allergic reaction to a COVID-19 vaccine or to any of the ingredients in the vaccine.")

        answerList[questionList[0]] = answer1
        answerList[questionList[1]] = answer2
        answerList[questionList[2]] = answer3
        answerList[questionList[3]] = answer4
        answerList[questionList[4]] = answer5
        answerList[questionList[5]] = answer6
        answerList[questionList[6]] = answer7
    }
}