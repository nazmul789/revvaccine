package com.example.revvaccine.view.certification

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.DatePicker
import android.widget.TimePicker
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.example.revvaccine.ClickerApplication
import com.example.revvaccine.R
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords
import com.example.revvaccine.databinding.FragmentUserMedicalBinding
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.models.MedicalRecordsModelFactory
import com.example.revvaccine.models.MedicalRecordsViewModel
import com.example.revvaccine.models.helpers.utils.navigateToHome
import com.google.android.material.textfield.TextInputEditText
import kotlinx.coroutines.launch
import timber.log.Timber
import java.lang.Exception


class UserMedicalInformation : Fragment() {

    private val medicalRecordsViewModel: MedicalRecordsViewModel by viewModels {
        MedicalRecordsModelFactory((context?.applicationContext as ClickerApplication).recordsRepository)
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        navigateToHome(requireContext(), findNavController(), R.id.nav_home)


        val binding: FragmentUserMedicalBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_user_medical, container, false)


        binding.lifecycleOwner?.let {
            medicalRecordsViewModel.allRecords.observe(it) { records ->
                // Update the cached copy of the words in the adapter.
                Toast.makeText(context, "found a word", Toast.LENGTH_SHORT).show()
            }
        }

        binding.medicalFormSubmit.setOnClickListener {

            if (inputIsValid(binding, binding.medicalFormEmail)
                and inputIsValid(binding, binding.medicalFormFirstName)
                and inputIsValid(binding, binding.medicalFormLastName)
                and inputIsValid(binding, binding.medicalFormSsn, length = 9)
                and inputIsValid(binding, binding.medicalFormPhoneNum, length = 10)
                and inputIsValid(binding, binding.medicalFormAddress)
                and inputIsValid(binding, binding.medicalFormPostal, length = 5)
                and inputIsValid(binding, binding.medicalFormState, length = 2)
            ) {

                val gender = if (binding.medicalFormGenderFemale.isChecked) "Female"
                else if (binding.medicalFormGenderMale.isChecked) "male"
                else "N/A"

                val birthDayPicker: DatePicker = binding.medicalFormAge
                val birthDay = birthDayPicker.dayOfMonth.toInt()
                val birthMonth = birthDayPicker.month.toInt() + 1
                val birthYear = birthDayPicker.year.toInt()

                //Invalid forms will disable the button, Assume that the form is valid
                val medicalInformation =
                    MedicalRecords(
                        userId = ApplicationModel.firebase.currentUser.uid,
                        email = binding.medicalFormEmail.text.toString(),
                        firstName = binding.medicalFormFirstName.text.toString(),
                        lastName = binding.medicalFormLastName.text.toString(),
                        phoneNum = binding.medicalFormPhoneNum.text.toString(),
                        ssn = binding.medicalFormSsn.text.toString(),
                        address = binding.medicalFormAddress.text.toString(),
                        postal = binding.medicalFormPostal.text.toString(),
                        state = binding.medicalFormState.text.toString(),
                        birthday = "$birthYear-$birthMonth-$birthDay",
                        gender = gender,
                        heartCondtion = binding.medicalCheckBoxHeart.isChecked,
                        cancer = binding.medicalCheckBoxCancer.isChecked,
                        hiv = binding.medicalCheckBoxHiv.isChecked,
                        obesity = binding.medicalCheckBoxWeight.isChecked,
                        pregnant = binding.medicalCheckBoxPregnancy.isChecked,
                        smoker = binding.medicalCheckBoxSmoking.isChecked,
                        diabetes = binding.medicalCheckBoxDiabetes.isChecked,
                        transplant = binding.medicalCheckBoxTransplant.isChecked,
                        immunocompromised = binding.medicalCheckBoxImmuno.isChecked,
                    )

                try {
                    lifecycleScope.launch {
                        medicalRecordsViewModel.insert(medicalInformation)
                        Toast.makeText(
                            context,
                            "Medical information recorded. Thank you!",
                            Toast.LENGTH_SHORT
                        ).show()
                        findNavController().navigate(R.id.vaccineCertification)
                    }
                } catch (e: Exception) {
                    Timber.i(e.printStackTrace().toString())
                }


            }

        }
        return binding.root

    }



    private fun inputIsValid(
        binding: FragmentUserMedicalBinding,
        inputEditText: TextInputEditText,
        length: Int = -1
    ): Boolean {
        val text = inputEditText.text.toString()
        if (text.isEmpty()) {
            inputEditText.error = "Field can't be empty"
            return false
        } else if (length > 0 && text.length != length) {
            inputEditText.error = "Field is not the proper length of $length"
            return false
        }
        inputEditText.error = null
        return true
    }

}
