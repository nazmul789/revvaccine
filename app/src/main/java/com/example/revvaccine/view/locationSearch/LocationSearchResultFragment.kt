package com.example.revvaccine.view.locationSearch

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.revvaccine.R
import com.example.revvaccine.models.LocationSearchViewModel
import com.example.revvaccine.models.helpers.locationSearch.*
import com.example.revvaccine.models.helpers.utils.ItemSwipeAction
import com.example.revvaccine.models.helpers.utils.LocationDistance
import timber.log.Timber


class LocationSearchResultFragment : Fragment() {
    private val LIST_SIZE = 35
    private val api by lazy {
        VaccineSpotterApi()
    }

    private val viewModel: LocationSearchViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_location_search_list, container, false)


        val openInGoogleMapButtom = view.findViewById<Button>(R.id.location_list_google_mode)
        val progressBar =
            view.findViewById<ProgressBar>(R.id.location_search_list_recyclerview_progress)
        val errorMessage = view.findViewById<TextView>(R.id.location_search_list_error)

        progressBar.visibility = ProgressBar.VISIBLE

        val state = viewModel.getState().value
        val postal = viewModel.getZipCode().value
        val latitude = viewModel.getLat().value
        val longitude = viewModel.getLong().value
        val radius = viewModel.getRadius().value

        Timber.i("STATE: $state, POSTAL: $postal, LAT: $latitude, LONG: $longitude, RADIUS: $radius")
        try {

            lifecycleScope.launchWhenCreated {
                api.call(
                    requireContext(),
                    state!!,
                    postal!!,
                    radius = radius!!
                )
            }

            api.error.observe(requireActivity(), Observer {
                if (it) {
                    errorMessage.visibility = TextView.VISIBLE
                    progressBar.visibility = ProgressBar.GONE
                    openInGoogleMapButtom.visibility = Button.GONE
                } else errorMessage.visibility = TextView.GONE

            })
            lifecycleScope.launchWhenStarted {

                api.getLocations().observe(requireActivity(), Observer { it ->
                    Timber.i("LOCATIONS ALL: ${it.size}")

                    openInGoogleMapButtom.setOnClickListener { view ->
                        viewModel.setALlLocations(it)
                        findNavController().navigate(R.id.googleMapsFragment)
                    }

                    //Filter locations with vaccine
                    var filtered =
                        it.filter { it.carriesVaccine.toBoolean() }
                    //Discard locations without appointment
                    filtered =
                        filtered.filter { it.appointmentsAvailable.toBoolean() }


                    // if "use my location" filters
                    if (latitude != null && longitude != null) {
                        openInGoogleMapButtom.visibility = Button.VISIBLE
                        Timber.i("USE MY LOCATIONe")

                        //Filter location < radius or 20 miles, then sort by distance
                        filtered = filtered.filter { location ->
                            val dist = LocationDistance(
                                latitude,
                                longitude,
                                location.latitude,
                                location.longitude
                            )
                            dist < radius ?: 30
                        }
                            //Sort by distance
                            .sortedBy { location ->
                                val dist = LocationDistance(
                                    latitude,
                                    longitude,
                                    location.latitude,
                                    location.longitude
                                )
                                dist
                            }
                    }else {
                        openInGoogleMapButtom.visibility = Button.GONE

                    }


                    filtered =
                        if (filtered.size > LIST_SIZE) filtered.slice(0 until LIST_SIZE) else filtered


                    Timber.i("final filtered size ${filtered.size}")


                    try {

                        val adapter =
                            LocationSearchResultListAdapter(
                                view.context,
                                filtered,
                                viewModel,
                                findNavController()
                            )
                        val locationRecyclerView =
                            view.findViewById<RecyclerView>(R.id.location_search_list_recyclerview)
                        locationRecyclerView.adapter = adapter

                        val swipeToDeleteCallback = object : ItemSwipeAction() {
                            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                                val pos = viewHolder.adapterPosition
                                adapter.notifyItemRemoved(pos)
                            }
                        }

                        val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
                        itemTouchHelper.attachToRecyclerView(locationRecyclerView)

                    }catch (e: Exception){
                        Timber.i(e.printStackTrace().toString())
                    }


                    progressBar.visibility = ProgressBar.GONE
                })
            }


        } catch (e: Exception) {
            Timber.e(e.printStackTrace().toString())
        }
        requireActivity().title = "Location Search Result"
        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // Hide Soft Keyboard
        val imm = activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


}