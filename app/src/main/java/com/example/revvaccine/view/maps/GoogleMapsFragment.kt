package com.example.revvaccine.view.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.revvaccine.R
import com.example.revvaccine.models.LocationSearchViewModel
import com.example.revvaccine.models.helpers.locationSearch.Location

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import timber.log.Timber

class GoogleMapsFragment : Fragment() {
    private val viewModel: LocationSearchViewModel by activityViewModels()
    private lateinit var map: GoogleMap
    private val REQUEST_LOCATION_PERMISSIN = 1
    private lateinit var progressBar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (viewModel.getALlLocations() == null) findNavController().navigate(R.id.locationView)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_google_maps, container, false)
        progressBar = view.findViewById(R.id.google_maps_progressBar)
        progressBar.visibility = ProgressBar.VISIBLE
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.google_maps_fragment) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)
    }

    private val callback = OnMapReadyCallback { googleMap ->
        progressBar.visibility = ProgressBar.GONE
        map = googleMap

        map.setInfoWindowAdapter(CustomInfoWindowAdapter())
        enableMyLocation()

        viewModel.getALlLocations()?.let { markMap(map, it) }

        val lat = viewModel.getLat().value
        val long = viewModel.getLong().value
        if (lat != null && long != null)
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, long), 100f))

    }


    private fun markMap(map: GoogleMap, list: List<Location>) {
        list.forEach {

            val options =
                MarkerOptions()
                    .position(LatLng(it.latitude, it.longitude))
                    .title(it.name)

            val iconId = when {
                it.appointmentsAvailable.toBoolean() -> R.drawable.map_syringe_emph
                else -> R.drawable.map_syringe_no_emph
            }
            val resourceDrawable = ContextCompat.getDrawable(requireContext(), iconId)
            val height = resourceDrawable?.intrinsicHeight
            val width = resourceDrawable?.intrinsicWidth
            if (height != null && width != null) {
                resourceDrawable!!.setBounds(0, 0, width, height)
                val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
                val canvas = Canvas(bitmap)
                resourceDrawable.draw(canvas)

                if (bitmap != null) options.icon(BitmapDescriptorFactory.fromBitmap(bitmap))

            }


            val marker = map.addMarker(options)
            marker.tag = it
        }

    }


    private fun enableMyLocation() {
        val isPermissionGranted =
            ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED

        if (isPermissionGranted) {
            map.isMyLocationEnabled = true
//            checkCurrentLocation()
            return
        }

        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_LOCATION_PERMISSIN
        )


    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_LOCATION_PERMISSIN) {
            if (grantResults.contains(PackageManager.PERMISSION_GRANTED)) {
//                checkCurrentLocation()
                map.isMyLocationEnabled = true
            } else {
                enableMyLocation()
            }
        }

    }



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.map_options, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.map_mode_normal -> {
            map.mapType = GoogleMap.MAP_TYPE_NORMAL
            true
        }
        R.id.map_mode_hybird -> {
            map.mapType = GoogleMap.MAP_TYPE_HYBRID
            true
        }
        R.id.map_mode_satellite -> {
            map.mapType = GoogleMap.MAP_TYPE_SATELLITE
            true
        }
        R.id.map_mode_terrain -> {
            map.mapType = GoogleMap.MAP_TYPE_TERRAIN
            true
        }
        else -> super.onOptionsItemSelected(item)


    }



    internal inner class CustomInfoWindowAdapter : GoogleMap.InfoWindowAdapter,
        GoogleMap.OnInfoWindowLongClickListener {
        private val contents =
            layoutInflater.inflate(R.layout.fragment_google_maps_marker_window, null)

        override fun getInfoWindow(marker: Marker): View? = null

        override fun getInfoContents(marker: Marker): View? {
            render(marker, contents)
            return contents
        }

        private fun render(marker: Marker, view: View) {
//            val badge = R.drawable.injection_approved
            val badgeView: ImageView = view.findViewById(R.id.google_maps_fragment_marker_badge)
            val titleView: TextView = view.findViewById(R.id.google_maps_fragment_marker_title)
            val descTopView: TextView =
                view.findViewById(R.id.google_maps_fragment_marker_description_top)
            val descMiddleView: TextView =
                view.findViewById(R.id.google_maps_fragment_marker_description_midlle)
            val descFooterView: TextView =
                view.findViewById(R.id.google_maps_fragment_marker_footer)


            if (marker.tag == null) {
                titleView.text = "Current location"

                badgeView.visibility = ImageView.GONE
                descTopView.visibility = TextView.GONE
                descMiddleView.visibility = TextView.GONE
                descFooterView.visibility = TextView.GONE
                return
            }

            badgeView.visibility = ImageView.VISIBLE
            descTopView.visibility = TextView.VISIBLE
            descMiddleView.visibility = TextView.VISIBLE
            descFooterView.visibility = TextView.VISIBLE

            val tag = marker.tag as Location

            val badge = when {
                tag.appointmentsAvailable.toBoolean() -> R.drawable.injection_approved
                tag.carriesVaccine.toBoolean() -> R.drawable.injection_approved
                else -> R.drawable.injection_denied
            }


            val snipAppt =
                if (tag.appointmentsAvailable.toBoolean()) "Appointment Available" else "No Appointments"
            val snipAll =
                if (tag.appointmentsAvailableAllDoses.toBoolean()) "All doses available" else ""
            val snipSecond =
                if (tag.appointmentsAvailableSecondDoseOnly.toBoolean()) "Second dose available" else ""

            val lastUpdated = tag.appointmentsLastFetched
            val coord = "${tag.latitude}, ${tag.longitude}"


            badgeView.setImageResource(badge)
            titleView.text = marker.title
            descTopView.text = coord
            descMiddleView.text =
                "$snipAppt ${if (snipAll.isNotEmpty()) "($snipAll)" else "($snipSecond)"}"

            descFooterView.text = lastUpdated

        }

        override fun onInfoWindowLongClick(marker: Marker?) {
            Timber.i("MAKER ON INFO .... ${marker?.tag}")
            if (marker?.tag != null) {
                val tag = marker.tag as Location
                val gmmIntentUri =
                    Uri.parse("geo:${tag.latitude},${tag.longitude}?q=" + Uri.encode(tag.address))
                val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                mapIntent.setPackage("com.google.android.apps.maps")
                ContextCompat.startActivity(requireContext(), mapIntent, null)
            }
        }
    }


}
