package com.example.revvaccine.view.certification

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.graphics.Paint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import com.example.revvaccine.ClickerApplication
import com.example.revvaccine.R
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocument
import com.example.revvaccine.databinding.FragmentVaccineCertificationBinding
import com.example.revvaccine.models.*
import com.example.revvaccine.models.helpers.utils.ScreenShot
import com.example.revvaccine.models.helpers.utils.navigateToHome
import kotlinx.android.synthetic.main.activity_main.*
import timber.log.Timber
import java.util.*

class VaccineCertification : Fragment() {
    private var vaccineRegisterData: List<VaccineVerificationDocument>? = null
    private var medicalRecordsData: List<MedicalRecords>? = null
    private var bookingsData: List<Bookings>? = null
    private lateinit var saveToFile: Button
    private val REQUEST_STORAGE_CODE = 1123
    private val PERMISSION_STORAGE = arrayOf<String>(
        Manifest.permission.READ_EXTERNAL_STORAGE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    )
    private val registerViewModel: VaccineRegistrationViewModel by viewModels {
        VaccineRegistrationFactory((context?.applicationContext as ClickerApplication).vaccineRegistrationRepository)
    }

    private val medicalRecordsViewModel: MedicalRecordsViewModel by viewModels {
        MedicalRecordsModelFactory((context?.applicationContext as ClickerApplication).recordsRepository)
    }
    private val bookingsViewModel: BookingsViewModel by viewModels {
        BookingsModelFactory((context?.applicationContext as ClickerApplication).bookingsRepository)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        navigateToHome(requireContext(), findNavController(), R.id.nav_home)

        val binding: FragmentVaccineCertificationBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_vaccine_certification,
            container,
            false
        )
        saveToFile = binding.certificationSaveToFileButton

        medicalRecordsViewModel.allRecords.observe(
            viewLifecycleOwner,
            Observer { t -> medicalRecordsData = t })
        registerViewModel.getAll()
            .observe(viewLifecycleOwner, Observer { t -> vaccineRegisterData = t })
        bookingsViewModel.getBookingsAll()
            .observe(viewLifecycleOwner, Observer { t -> bookingsData = t })


        binding.vaccineCertificationInputSubmit.setOnClickListener {
            cleanup(binding)
            val ssn: String = binding.vaccineCertificationInputSsn.text.toString()
            if (ssn.isNullOrEmpty() || ssn.length != 9) {
                binding.vaccineCertificationInputSsn.error = "Field is not the proper length of 9"
            } else {
                if (vaccineRegisterData != null) {
                    Timber.i("vaccineRegisterData-----> $vaccineRegisterData")
                    val recordFiltered = vaccineRegisterData?.filter { doc -> doc.ssn == ssn }
                    if (recordFiltered != null && recordFiltered.isNotEmpty()) {
                        val record = recordFiltered[0]
                        when (record.type) {
                            "MODERNA" -> binding.certificationTextVaccineType.text = "Moderna"
                            "JNJ" -> binding.certificationTextVaccineType.text = "Johnson & Johnson"
                            "PFIZER" -> binding.certificationTextVaccineType.text = "Pfizer"
                            "UNKNOWN" -> binding.certificationTextVaccineType.text = "Unknown"
                        }
                        binding.certificationTextReference.text = record.id.toString()
                    }
                }

                if (medicalRecordsData != null) {
                    Timber.i("medicalRecordsData-----> $medicalRecordsData")

                    val recordFiltered = medicalRecordsData?.filter { doc -> doc.ssn == ssn }
                    if (recordFiltered != null && recordFiltered.isNotEmpty()) {
                        val record = recordFiltered[0]
                        binding.certificationTextFirstName.text = record.firstName
                        binding.certificationTextLastName.text = record.lastName
                        binding.certificationTextDob.text = record.birthday
                    }
                }


                if (bookingsData != null) {
                    Timber.i("bookingsData-----> $bookingsData")

                    val recordFiltered = bookingsData?.filter { doc -> doc.ssn == ssn }
                    if (recordFiltered != null && recordFiltered.isNotEmpty()) {
                        val record = recordFiltered[0]
                        binding.certificationTextReference.text = record.referenceId
                    }
                }
            }
        }



        saveToFile.setOnClickListener {
            requestStoragePermission(requireActivity())
        }

        // Inflate the layout for this fragment
        return binding.root
    }


    private fun cleanup(binding: FragmentVaccineCertificationBinding) {
        binding.certificationTextFirstName.text = "Not Found"
        binding.certificationTextLastName.text = "Not Found"
        binding.certificationTextDob.text = "Not Found"
        binding.certificationTextReference.text = "Not Found"
        binding.certificationTextVaccineType.text = "Not Found"
//        binding.certificationTextDateOfDoseOne.text = "not found"
//        binding.certificationTextDateOfDoseTwo.text = "not found"
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_STORAGE_CODE) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                ScreenShot.screenShot(requireActivity(), requireActivity().window.decorView)
            }
        }

    }


    fun requestStoragePermission(activity: Activity?) {
        val permission = ActivityCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                PERMISSION_STORAGE,
                REQUEST_STORAGE_CODE
            )
        } else {
            ScreenShot.screenShot(requireActivity(), requireActivity().window.decorView)
        }
    }
}