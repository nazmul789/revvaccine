package com.example.revvaccine.view.navbar

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.revvaccine.R


class SubmitFeedback : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_submit_feedback, container, false)

        val sendEmailTech = view.findViewById<Button>(R.id.report_technical_issue)
        val sendEmailGeneral = view.findViewById<Button>(R.id.general_feedback)

        sendEmailTech.setOnClickListener { emailTech() }
        sendEmailGeneral.setOnClickListener { emailGeneral() }

        return view
    }

    private fun emailTech() {
        startActivity(Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:glorial0918@gmail.com")))

        try {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_APP_EMAIL)
            this.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                getActivity(),
                "No email application found.",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    private fun emailGeneral() {
        startActivity(Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:glorial0918@gmail.com")))

        try {
            val intent = Intent(Intent.ACTION_MAIN)
            intent.addCategory(Intent.CATEGORY_APP_EMAIL)
            this.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(
                getActivity(),
                "No email application found.",
                Toast.LENGTH_LONG
            ).show()
        }
    }
}
