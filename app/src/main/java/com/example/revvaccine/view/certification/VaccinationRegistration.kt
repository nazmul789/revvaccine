package com.example.revvaccine.view.certification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import camera.CameraManager
import camera.CameraWrapper
import com.example.revvaccine.ClickerApplication
import com.example.revvaccine.R
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocument
import com.example.revvaccine.databinding.VaccinationRegistrationFragmentBinding
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.models.VaccineRegistrationFactory
import com.example.revvaccine.models.VaccineRegistrationViewModel
import com.example.revvaccine.models.helpers.utils.navigateToHome
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber


class VaccinationRegistration : Fragment() {
    private lateinit var cameraManager: CameraManager
    private var ssn: String = ""
    private var type: String = ""
    private var proofOfIdentity: Boolean = false
    private var proofOfShotOne: Boolean = false
    private var proofOfShotTwo: Boolean = false

    private val viewModel: VaccineRegistrationViewModel by viewModels {
        VaccineRegistrationFactory((context?.applicationContext as ClickerApplication).vaccineRegistrationRepository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        navigateToHome(requireContext(),findNavController(), R.id.nav_home)


        val binding: VaccinationRegistrationFragmentBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.vaccination_registration_fragment,
            container,
            false
        )

        //init
        cameraManager = CameraManager(activity as AppCompatActivity, binding.root)


        //shot type radio listener
        binding.vaccinationRegistrationTypeRadio.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.vaccination_registration_type_jnj -> type = "JNJ"
                R.id.vaccination_registration_type_moderna -> type = "MODERNA"
                R.id.vaccination_registration_type_pfizer -> type = "PFIZER"
                else -> "UNKNOWN"
            }
        }
        /**
         * Assumptions:
         *  - We assume that the photo id is always valid.
         *  Realistically, Photo proofs will go to a real person and they will review and approve
         */
        //Photo id listener
        binding.vaccinationRegistrationButtonPhotoId.setOnClickListener {
            val cameraWrapper = CameraWrapper(requireActivity())
            cameraManager.permissionAndImageStarter {
                if (cameraWrapper.startPictureEvent()) {
                    proofOfIdentity = true
                    binding.vaccinationRegistrationTextPhotoId.setBackgroundColor(resources.getColor(R.color.teal_700))
                }
            }
        }
        //shot one listener
        binding.vaccinationRegistrationButtonShotOne.setOnClickListener {
            val cameraWrapper = CameraWrapper(requireActivity())
            cameraManager.permissionAndImageStarter {
                val success = cameraWrapper.startPictureEvent()
                if (success) {
                    proofOfShotOne = true

                    binding.vaccinationRegistrationTextShotOne.setBackgroundColor(resources.getColor(R.color.teal_700))

                }
            }

        }
        //shot two listener
        binding.vaccinationRegistrationButtonShotTwo.setOnClickListener {
            val cameraWrapper = CameraWrapper(requireActivity())
            cameraManager.permissionAndImageStarter {
                val success = cameraWrapper.startPictureEvent()
                if (success) {
                    proofOfShotTwo = true
                    binding.vaccinationRegistrationTextShotTwo.setBackgroundColor(resources.getColor(R.color.teal_700))
                }
            }
        }


        //ssn listener
        binding.vaccinationRegistrationSsn.doAfterTextChanged { text ->
            ssn = text.toString()
        }

        //Submit button
        binding.vaccinationRegistrationSubmitButton.setOnClickListener {
            if (isValid(binding)) {
                Snackbar.make(
                    requireView(),
                    "Upload successful, Please await approval",
                    Snackbar.LENGTH_LONG
                ).show()

                Timber.i("ssn ---> $ssn")
                Timber.i("type ---> $type")
                Timber.i("id ---> $proofOfIdentity")
                Timber.i("one ---> $proofOfShotOne")
                Timber.i("two ---> $proofOfShotTwo")
                val doc = VaccineVerificationDocument(
                    userId = ApplicationModel.firebase.currentUser.uid,
                    type=type,
                    ssn = ssn,
                    photoId = proofOfIdentity,
                    shotOne = proofOfShotOne,
                    shotTwo = proofOfShotTwo
                )
                viewModel.insert(doc)
                findNavController().navigate(R.id.action_vaccinationRegistration_to_vaccineCertification)
            } else {
                Snackbar.make(requireView(), "Invalid submission.", Snackbar.LENGTH_SHORT).show()
            }
        }


        return binding.root


    }

    private fun isValid(binding: VaccinationRegistrationFragmentBinding): Boolean {
        if(ssn.length != 9){
            binding.vaccinationRegistrationSsn.error = "Field must be length of 9"
            return false
        } else {
            binding.vaccinationRegistrationSsn.error = null
        }
        if (type.isEmpty()) return false
        if (!proofOfIdentity) return false

        when (type) {
            "JNJ" -> if (!proofOfShotOne) return false
            "MODERNA" -> if (!proofOfShotOne || !proofOfShotTwo) return false
            "PFIZER" -> if (!proofOfShotOne || !proofOfShotTwo) return false
            else -> return false
        }

        return true
    }

}