package com.example.revvaccine.view.locationSearch

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ProgressBar
import android.widget.Spinner
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.example.revvaccine.R
import com.example.revvaccine.databinding.FragmentLocationSearchFormBinding
import com.example.revvaccine.models.LocationSearchViewModel
import com.example.revvaccine.models.helpers.utils.States
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_location_search_form.*
import timber.log.Timber
import java.sql.Time
import java.util.*
import kotlin.Exception


class LocationView : Fragment(R.layout.fragment_location_search_form), LocationListener {
    private val viewModel: LocationSearchViewModel by activityViewModels()
    private lateinit var binding: FragmentLocationSearchFormBinding
    private val permissionFine = Manifest.permission.ACCESS_FINE_LOCATION
    private val permissionCoarse = Manifest.permission.ACCESS_COARSE_LOCATION
    private val locationPermission = 4
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        var spin: Spinner = view.findViewById(R.id.location_search_form_states)
        ArrayAdapter.createFromResource(
            this.requireContext(),
            R.array.states,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_item)
            spin.adapter = adapter
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding =
            DataBindingUtil.inflate(
                inflater,
                R.layout.fragment_location_search_form, container, false
            )
        binding.locationSearchFormProgressBar.visibility = ProgressBar.GONE
        binding.locationSearchFormMyLocation.setOnClickListener {
            binding.locationSearchFormProgressBar.visibility = ProgressBar.VISIBLE
            requestLocationPermission()
        }

        binding.locationSearchFormButtonSearch.setOnClickListener {
            if (inputIsValid(binding.locationSearchFormDistance)
                and inputIsValid(binding.locationSearchFormPostal, length = 5)
            ) {
                binding.locationSearchFormProgressBar.visibility = ProgressBar.VISIBLE
                viewModel.longitude.value = null
                viewModel.latitude.value = null
                val testString = "zip:${binding.locationSearchFormPostal.text.toString()} " +
                        " City: ${binding.locationSearchFormDistance.text.toString()}" +
                        " State: ${location_search_form_states.selectedItem.toString()}"
                Toast.makeText(this.context, testString, Toast.LENGTH_LONG).show()
                val selectedState =
                    binding.locationSearchFormStates.selectedItem.toString().trim().substring(0, 2)

                viewModel.setRadius(binding.locationSearchFormDistance.text.toString().toInt())
                viewModel.setState(selectedState)
                viewModel.setZipCode(binding.locationSearchFormPostal.text.toString())
                findNavController().navigate(R.id.action_locationView_to_locationSearchResultFragment)

            }
        }

        return binding.root
    }


    private fun inputIsValid(
        inputEditText: TextInputEditText,
        length: Int = -1
    ): Boolean {
        val text = inputEditText.text.toString()
        if (text.isEmpty()) {
            inputEditText.error = "Field can't be empty"
            return false
        } else if (length > 0 && text.length != length) {
            inputEditText.error = "Field is not the proper length of $length"
            return false
        }
        inputEditText.error = null
        return true
    }

    private fun requestLocationPermission() {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                permissionFine
            ) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(
                requireContext(),
                permissionCoarse
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                requireActivity(),
                arrayOf(permissionCoarse, permissionFine),
                locationPermission
            )
            binding.locationSearchFormProgressBar.visibility = ProgressBar.GONE
        } else {
            requestLocationAndGo()
        }

    }

    private var locationManager: LocationManager? = null

    @SuppressLint("MissingPermission")
    private fun requestLocationAndGo() {
        try {

            locationManager =
                requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
            var gpsEnabled = false
            var networkEnabled = false

            try {
                gpsEnabled = locationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)
            } catch (e: Exception) {
            }

            try {
                networkEnabled =
                    locationManager!!.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            } catch (e: Exception) {
            }

            if (!gpsEnabled && !networkEnabled) {
                showLocationDisabledDialog()
            } else {

                Timber.d("location is null? ${locationManager.toString()}")
                locationManager!!.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    5000,
                    5f,
                    this
                )
                locationManager!!.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER,
                    5000,
                    5f,
                    this
                )
            }


        } catch (e: Exception) {
            Timber.i(e.printStackTrace().toString())
        }

    }

    private fun showLocationDisabledDialog() {
        binding.locationSearchFormProgressBar.visibility = ProgressBar.GONE
        val builder = AlertDialog
            .Builder(requireContext())
        builder
            .setMessage("Location not enabled, please enable location")
            .setPositiveButton("Setting",
                DialogInterface.OnClickListener { dialog, id ->
                    requireActivity().startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                })
            .setNegativeButton("Cancel", null)
            .show()
    }

    override fun onLocationChanged(location: Location) {
        val latitude = location.latitude
        val longitude = location.longitude

        val geocoder = Geocoder(context, Locale.getDefault());
        val reverseAddress = geocoder.getFromLocation(latitude, longitude, 1)[0]

        Timber.i(reverseAddress.toString())
        val postal = reverseAddress.postalCode
        val stateAbbreviation = States[reverseAddress.adminArea]

        if (stateAbbreviation != null) {
            viewModel.setState(stateAbbreviation)
            viewModel.setZipCode(postal)
            viewModel.setLat(latitude)
            viewModel.setLong(longitude)
            findNavController().navigate(R.id.locationSearchResultFragment)
        } else {
            Toast.makeText(
                requireContext(),
                "Failed to use your location, please try again or enter manually",
                Toast.LENGTH_LONG
            ).show()
        }
    }

    override fun onProviderEnabled(provider: String) {
        Timber.d("onProviderEnabled location GPS proider  is? ${provider.toString()}")

    }

    override fun onProviderDisabled(provider: String) {
        showLocationDisabledDialog()
        Timber.d("onProviderDisabled   proider  is? ${provider.toString()}")
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        Timber.d("onStatusChanged   proider  is? ${provider.toString()}")
        Timber.d("onStatusChanged   status  is? ${status.toString()}")
    }

    override fun onPause() {
        super.onPause()
        Timber.i("LocationView onPause")
        locationManager?.removeUpdates(this)

    }

    override fun onDestroy() {
        Timber.i("LocationView onDestory")
        super.onDestroy()
    }
}