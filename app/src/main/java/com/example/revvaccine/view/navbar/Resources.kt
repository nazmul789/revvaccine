package com.example.revvaccine.view.navbar

import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.revvaccine.R
import kotlinx.android.synthetic.*


class Resources : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_resources, container, false)

        val link_cdc = view.findViewById(R.id.linkCDC) as TextView
        link_cdc.movementMethod = LinkMovementMethod.getInstance()

        val link_fda = view.findViewById(R.id.linkFDA) as TextView
        link_fda.movementMethod = LinkMovementMethod.getInstance()

        val link_usda = view.findViewById(R.id.linkUSDA) as TextView
        link_usda.movementMethod = LinkMovementMethod.getInstance()

        val link_dol = view.findViewById(R.id.linkDOL) as TextView
        link_dol.movementMethod = LinkMovementMethod.getInstance()

        val link_benefits = view.findViewById(R.id.linkBenefits) as TextView
        link_benefits.movementMethod = LinkMovementMethod.getInstance()

        val link_tjc = view.findViewById(R.id.linkTJC) as TextView
        link_tjc.movementMethod = LinkMovementMethod.getInstance()

        return view
    }
}
