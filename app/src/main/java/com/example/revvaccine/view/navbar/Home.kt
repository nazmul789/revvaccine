package com.example.revvaccine.view.navbar

import android.app.Activity
import android.content.Intent
import android.content.res.Resources
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.revvaccine.R
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.server.Requests
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.IdpResponse
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import timber.log.Timber

private const val RC_SIGN_IN = 123


class Home : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        val view = inflater.inflate(R.layout.fragment_home, container, false)


        val authButton = view.findViewById<Button>(R.id.app_home_auth_button)
        val noAuthButton = view.findViewById<TextView>(R.id.app_home_no_auth_button)




        noAuthButton.setOnClickListener {
//            view.findViewById<View>(R.id.bottom_navigation).visibility = View.INVISIBLE
            findNavController().navigate(R.id.action_home2_to_locationView)
        }

        noAuthButton.setOnClickListener { findNavController().navigate(R.id.locationView) }


        authButton.setOnClickListener { signIn() }
        return view
    }

    override fun onStart() {
        super.onStart()
        if (ApplicationModel.isLoggedIn()) requireActivity().findNavController(R.id.NavHostFragment)
            .navigate(R.id.locationView)
    }

    // Choose authentication providers
    private fun signIn() {

        val idpConfigs = listOf(
            AuthUI.IdpConfig.GoogleBuilder().build(),
            AuthUI.IdpConfig.EmailBuilder().build(),
            AuthUI.IdpConfig.PhoneBuilder().build(),
        )

        // Create and launch sign-in intent
        val intent = AuthUI.getInstance()
            .createSignInIntentBuilder()
            .setAvailableProviders(idpConfigs)
            .setLogo(R.drawable.revvac_logo)
//            .build()

        try {
            intent
                .setTheme(R.style.GreenTheme)
        } catch (e: java.lang.IllegalArgumentException) {
            Timber.e(e.printStackTrace().toString())
        } catch (e: Exception) {
            Timber.e(e.printStackTrace().toString())
        }

        startActivityForResult(intent.build(), RC_SIGN_IN)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == Activity.RESULT_OK) {
                val user = ApplicationModel.firebase.currentUser
                Timber.i("USER SIGNED IN USING UI ----> ${user.providerData}")

                requireActivity().navView.menu.findItem(R.id.nav_logOut).isVisible =
                    ApplicationModel.isLoggedIn()

                GlobalScope.launch {
                    Requests.syncWithBackEnd(ApplicationModel.firebase.currentUser.uid)
                }

                findNavController()
                    .navigate(R.id.locationView)

            } else {
                val response = IdpResponse.fromResultIntent(data)
                response?.error?.printStackTrace()
                Timber.i("USER SIGNED IN USING UI ----> FAIL")
            }
        }
    }
}
