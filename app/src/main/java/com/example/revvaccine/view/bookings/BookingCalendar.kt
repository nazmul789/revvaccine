package com.example.revvaccine.view.bookings

import android.app.DatePickerDialog
import android.app.NotificationManager
import android.app.TimePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.annotation.RequiresApi
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.revvaccine.ClickerApplication
import com.example.revvaccine.R
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import com.example.revvaccine.databinding.FragmentBookingCalendarBinding
import com.example.revvaccine.models.*
import com.example.revvaccine.models.helpers.bookingList.BookingDataViewModel
import com.example.revvaccine.models.helpers.notification.sendBookingNotification
import com.example.revvaccine.models.helpers.utils.navigateToHome
import kotlinx.android.synthetic.main.fragment_booking_calendar.*
import java.util.*
import kotlin.random.Random

class BookingCalendar : Fragment(), DatePickerDialog.OnDateSetListener,
    TimePickerDialog.OnTimeSetListener {
    private val bookingDataViewModel: BookingDataViewModel by activityViewModels()

    private val viewModel: LocationSearchViewModel by activityViewModels()

    private val bookingsViewModel: BookingsViewModel by viewModels {
        BookingsModelFactory((context?.applicationContext as ClickerApplication).bookingsRepository)
    }

    private val medicalRecordsViewModel: MedicalRecordsViewModel by viewModels {
        MedicalRecordsModelFactory((context?.applicationContext as ClickerApplication).recordsRepository)
    }

    private lateinit var dateTimeLabel: TextView
    private lateinit var btnPick: Button
    var day = 0
    var month = 0
    var year = 0
    var hour = 0
    var minute = 0
    var myday = 0
    var myMonth = 0
    var myYear = 0
    var myHour = 0
    var myMinute = 0

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        navigateToHome(requireContext(),findNavController(), R.id.nav_home)

        val binding: FragmentBookingCalendarBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_booking_calendar, container, false)

        binding.bookingFirstname.setText(bookingDataViewModel.fistName)
        binding.bookingLastname.setText(bookingDataViewModel.lastName)
        binding.bookingReferenceId.setText(bookingDataViewModel.refID)
        binding.bookingFormSsn.setText(bookingDataViewModel.ssn)
        dateTimeLabel = binding.bookingDateTimeLabel
        btnPick = binding.btnPick

        btnPick.setOnClickListener(object : View.OnClickListener {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onClick(v: View?) {
                val calendar: Calendar = Calendar.getInstance()
                year = calendar.get(Calendar.YEAR)
                month = calendar.get(Calendar.MONTH)
                day = calendar.get(Calendar.DAY_OF_MONTH)
                val datePickerDialog =
                    DatePickerDialog(context!!, this@BookingCalendar, year, month, day)
                datePickerDialog.show()
            }
        })

        binding.bookingFirstname.doAfterTextChanged { text ->
            bookingDataViewModel.fistName = text.toString()
        }
        binding.bookingLastname.doAfterTextChanged { text ->
            bookingDataViewModel.lastName = text.toString()
        }
        binding.bookingReferenceId.doAfterTextChanged { text ->
            bookingDataViewModel.refID = text.toString()
        }
        binding.bookingFormSsn.doAfterTextChanged { text ->
            bookingDataViewModel.ssn = text.toString()
        }


        binding.bookingSpeedUpButton.setOnClickListener {
            findNavController().navigate(R.id.action_bookingCalendar_to_userMedicalInformation)
        }

        binding.bookingConfirmButton.setOnClickListener {
            if (bookingDataViewModel.fistName.isEmpty() ||
                bookingDataViewModel.lastName.isEmpty() ||
                bookingDataViewModel.refID.isEmpty() ||
                bookingDataViewModel.ssn.isEmpty() ||
                bookingDataViewModel.appointmentDate.isEmpty() ||
                bookingDataViewModel.appointmentTime.isEmpty()
            ) {
                Toast.makeText(context, "Missing Fields", Toast.LENGTH_SHORT).show()
            } else {

                val records =
                    medicalRecordsViewModel.getRecordsBySSN(bookingDataViewModel.ssn).value.toString()

                var buildBookings: Bookings? = null
                if (bookingDataViewModel.bookings != null) {
                    buildBookings = bookingDataViewModel.bookings
                    bookingDataViewModel.bookings = null

                    buildBookings?.ssn = bookingDataViewModel.ssn
                    buildBookings?.firstName = bookingDataViewModel.fistName
                    buildBookings?.lastName = bookingDataViewModel.lastName
                    buildBookings?.date = bookingDataViewModel.appointmentDate
                    buildBookings?.referenceId = Random.nextInt(0, 9999999).toString()
                    buildBookings?.time = bookingDataViewModel.appointmentTime


                } else {
                    val loc = viewModel.getSelectedLocation().value
                    buildBookings = Bookings(
                        userId = ApplicationModel.firebase.currentUser.uid,
                        ssn = bookingDataViewModel.ssn,
                        firstName = bookingDataViewModel.fistName,
                        lastName = bookingDataViewModel.lastName,
                        date = bookingDataViewModel.appointmentDate,
                        referenceId = Random.nextInt(0, 9999999).toString(),
                        time = bookingDataViewModel.appointmentTime,
                        locationName = loc?.name!!,
                        locationAddress = loc.address,
                        locationCity = loc.city,
                        locationState = loc.state,
                        locationPostal = loc.postalCode,
                        locationLatitude = loc.latitude.toString(),
                        locationLongitude = loc.longitude.toString(),
                        locationVaccineType = when {
                            loc.jnjAvaiable -> "Johnson and Johnson"
                            loc.modernaAvailable -> "Moderna"
                            loc.pfizerAvailable -> "Pfizer"
                            loc.unknownAvailable -> "Unknown"
                            else -> "N/A"
                        },
                        medicalRecords = records,
                        appointmentApproved = Random.nextBoolean()
                    )
                }

                if (buildBookings != null) {
                    bookingsViewModel.insert(buildBookings)
                }


                bookingDataViewModel.calendar.set(myYear, myHour, myday, myHour, myMinute)

                val notificationManager: NotificationManager =
                    requireContext().getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                notificationManager.sendBookingNotification(
                    requireContext(),
                    title = "Vaccine appointment",
                    messageBody = "You have an appointment on $myMonth/$myday/$myYear $myHour : $myMinute",
                    setWhen = bookingDataViewModel.calendar.timeInMillis,
                )
                findNavController().navigate(R.id.action_bookingCalendar_to_bookingsListFragment)
            }
        }




        return binding.root


    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        myYear = year
        myday = day
        myMonth = month
        val c: Calendar = Calendar.getInstance()
        hour = c.get(Calendar.HOUR)
        minute = c.get(Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(
            context,
            this@BookingCalendar,
            hour,
            minute,
            DateFormat.is24HourFormat(context)
        )
        timePickerDialog.show()
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        myHour = hourOfDay
        myMinute = minute
        bookingDataViewModel.appointmentDate = """$myMonth/$myday/$myYear""".trimIndent()
        bookingDataViewModel.appointmentTime = """$myHour : $myMinute""".trimIndent()

        dateTimeLabel.text= "$myMonth/$myday/$myYear $myHour : $myMinute"
    }
}