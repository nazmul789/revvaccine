package com.example.revvaccine.view.bookings

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.revvaccine.ClickerApplication
import com.example.revvaccine.R
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.models.helpers.bookingList.BookingAdapter
import com.example.revvaccine.models.BookingsModelFactory
import com.example.revvaccine.models.BookingsViewModel
import com.example.revvaccine.models.helpers.bookingList.BookingDataViewModel
import com.example.revvaccine.models.helpers.utils.ItemSwipeAction
import com.example.revvaccine.models.helpers.utils.navigateToHome
import com.google.android.material.snackbar.Snackbar
import timber.log.Timber


/**
Class to inflate all booking items to create a booking list

Reference: In java but it translates over https://www.geeksforgeeks.org/swipe-to-delete-and-undo-in-android-recyclerview/
 */
class BookingsListFragment : Fragment() {
    private val bookingDataViewModel: BookingDataViewModel by activityViewModels()

    private val viewModel: BookingsViewModel by viewModels {
        BookingsModelFactory((context?.applicationContext as ClickerApplication).bookingsRepository)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        navigateToHome(requireContext(),findNavController(), R.id.nav_home)


        val view = inflater.inflate(R.layout.fragment_bookings_list, container, false)
        val notFoundMessage = view.findViewById<TextView>(R.id.bookings_list_not_found_message)
        val recyclerView: RecyclerView = view.findViewById(R.id.recyclerViewBooking)


        notFoundMessage.visibility = TextView.VISIBLE
        viewModel.getBookingsAll().observe(viewLifecycleOwner, Observer { bookings ->
            if (bookings.isNotEmpty()) {
                Timber.i("BOOKINGS SIZE:${bookings.size}" )

                val adapter = BookingAdapter(requireContext(), findNavController(),bookings, bookingDataViewModel)
                recyclerView.adapter = adapter
                recyclerView.layoutManager = LinearLayoutManager(view?.context)
                recyclerView.setHasFixedSize(false)


                val swipeToDeleteCallback = object : ItemSwipeAction() {
                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        val pos = viewHolder.adapterPosition
                        val removedBooking = bookings[pos]
                        viewModel.deleteByRefId(removedBooking.referenceId)
                        Snackbar.make(
                            view,
                            "Booking at ${removedBooking.date} ${removedBooking.time} removed",
                            Snackbar.LENGTH_LONG
                        )
                            .setAction("Undo") {
                                viewModel.insert(removedBooking)
                            }.show()
                        adapter.notifyItemRemoved(pos)
                    }
                }

                val itemTouchHelper = ItemTouchHelper(swipeToDeleteCallback)
                itemTouchHelper.attachToRecyclerView(recyclerView)
                notFoundMessage.visibility = TextView.GONE

                recyclerView.visibility= RecyclerView.VISIBLE
            } else {
                notFoundMessage.visibility = TextView.VISIBLE
                recyclerView.visibility= RecyclerView.GONE
            }
        })

        return view
    }
}