package com.example.revvaccine.server.service

import com.example.revvaccine.dataBase.BookingsTable.Bookings
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT


interface BookingsDAO {

    @POST("api/booking/sync/retrieve")
    suspend fun get(@Body data: SyncRetrieveBody): List<Bookings>

    @POST("api/booking/sync/send")
    suspend fun sync(@Body data: SyncBody<Bookings>)

    @POST("api/booking")
    suspend fun post(): List<Bookings>

    @PUT("api/booking")
    suspend fun put(): List<Bookings>


}

