package com.example.revvaccine.server.service


import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocument
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT


interface VaccineVerificationDocumentDAO {

    @POST("api/certificate/sync/retrieve")
    suspend fun get(@Body data: SyncRetrieveBody):  List<VaccineVerificationDocument>

    @POST("api/certificate/sync/send")
    suspend fun sync(@Body data: SyncBody<VaccineVerificationDocument>)

    @POST("api/certificate")
    suspend fun post(): List<VaccineVerificationDocument>

    @PUT("api/certificate")
    suspend fun put(): List<VaccineVerificationDocument>


}
