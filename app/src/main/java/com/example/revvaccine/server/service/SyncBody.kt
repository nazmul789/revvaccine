package com.example.revvaccine.server.service

data class SyncBody <T> (val uid: String, val data: List<T>)
data class SyncRetrieveBody (val uid: String)
