package com.example.retrofitmoshitest.backend



import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords
import com.example.revvaccine.server.service.SyncBody
import com.example.revvaccine.server.service.SyncRetrieveBody
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT


interface MedicalRecordsDAO {

    @POST("api/medical/sync/retrieve")
    suspend fun get(@Body data: SyncRetrieveBody): List<MedicalRecords>

    @POST("api/medical/sync/send")
    suspend fun sync(@Body data: SyncBody<MedicalRecords>)

    @POST("api/medical")
    suspend fun post(): List<MedicalRecords>

    @PUT("api/medical")
    suspend fun put(): List<MedicalRecords>


}
