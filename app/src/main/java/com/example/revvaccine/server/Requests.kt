package com.example.revvaccine.server

import androidx.lifecycle.ViewModel
import com.example.revvaccine.ClickerApplication
import com.example.revvaccine.dataBase.BookingsTable.BookingsRepository
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecordsRepository
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocumentRepository
import com.example.revvaccine.server.service.SyncAPI
import com.example.revvaccine.server.service.SyncBody
import com.example.revvaccine.server.service.SyncRetrieveBody
import timber.log.Timber

object Requests {


    private lateinit var bookingRepo: BookingsRepository
    private lateinit var recordRepo: MedicalRecordsRepository
    private lateinit var certificateRepo: VaccineVerificationDocumentRepository

    fun init(app: ClickerApplication) {
        bookingRepo = app.bookingsRepository
        recordRepo = app.recordsRepository
        certificateRepo = app.vaccineRegistrationRepository

    }

    suspend fun syncWithBackEnd(uid: String) {
        try {
            val bookings = SyncAPI.booking.get(SyncRetrieveBody(uid))
            val records = SyncAPI.medicalRecords.get(SyncRetrieveBody(uid))
            val certificate = SyncAPI.certificate.get(SyncRetrieveBody(uid))

            Timber.i(bookings.toString())

            bookings.forEach { bookingRepo.insert(it) }
            certificate.forEach { certificateRepo.insert(it) }
            records.forEach { recordRepo.insert(it) }
        } catch (e: Exception) {

            Timber.i(e.printStackTrace().toString())
        }
    }


    suspend fun certifiateToBackend(uid: String) {
        try {

            val allCerti = certificateRepo.getBookingsAllDead()
            SyncAPI.certificate.sync(SyncBody(uid = uid, data = allCerti))
        } catch (e: Exception) {

            Timber.i(e.printStackTrace().toString())
        }

    }

    suspend fun recordToBackend(uid: String) {
        try {

            val allRecords = recordRepo.allRecordsDead()
            SyncAPI.medicalRecords.sync(
                SyncBody(
                    uid = uid,
                    data = allRecords
                )
            )
        } catch (e: Exception) {

            Timber.i(e.printStackTrace().toString())
        }

    }

    suspend fun bookingToBackend(uid: String) {
        try {

            val allBookings = bookingRepo.getBookingsAllDead()
            SyncAPI.booking.sync(SyncBody(uid = uid, data = allBookings))
        } catch (e: Exception) {

            Timber.i(e.printStackTrace().toString())
        }
    }

    suspend fun clearLocal() {
        bookingRepo.deleteAll()
        recordRepo.deleteAll()
        certificateRepo.deleteAll()
    }
}