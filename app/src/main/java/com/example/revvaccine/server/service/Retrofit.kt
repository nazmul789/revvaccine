package com.example.revvaccine.server.service
import com.example.retrofitmoshitest.backend.MedicalRecordsDAO
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

private const val BASE_URL = "https://revvaccine.herokuapp.com/"




 val moshi: Moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

val retrofit: Retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()


object SyncAPI {
    val booking : BookingsDAO by lazy { retrofit.create(BookingsDAO::class.java) }
    val medicalRecords : MedicalRecordsDAO by lazy { retrofit.create(MedicalRecordsDAO::class.java) }
    val certificate : VaccineVerificationDocumentDAO by lazy { retrofit.create(VaccineVerificationDocumentDAO::class.java) }
}
