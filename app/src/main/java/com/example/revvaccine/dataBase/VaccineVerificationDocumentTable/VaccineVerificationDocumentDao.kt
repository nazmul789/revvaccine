package com.example.revvaccine.dataBase.VaccineVerificationDocumentTable

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.revvaccine.dataBase.BookingsTable.Bookings

@Dao
interface VaccineVerificationDocumentDao {

    @Query("SELECT * FROM Vaccine_Verification_Document")
    fun getVerification(): LiveData<List<VaccineVerificationDocument>>

    @Query("SELECT * FROM Vaccine_Verification_Document")
   suspend fun getVerificationDead(): List<VaccineVerificationDocument>

    @Query("SELECT * FROM Vaccine_Verification_Document WHERE ssn = :ssn")
    fun getVerificationBySSN(ssn: String): LiveData<VaccineVerificationDocument>


    @Query("SELECT * FROM Vaccine_Verification_Document AS V INNER JOIN medical_records_information AS R ON V.ssn = R.ssn INNER JOIN Bookings AS B ON V.ssn=b.ssn")
    fun collectDocuments(): LiveData<List<VaccineVerificationDocument>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(records: VaccineVerificationDocument)

    @Query("DELETE FROM Vaccine_Verification_Document")
    suspend fun deleteAll()

    @Query("DELETE FROM Vaccine_Verification_Document WHERE ssn = :ssn")
    suspend fun deleteBySsn(ssn: String)
}