package com.example.revvaccine.dataBase.MedicalRecordsTable

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "medical_records_information", indices = [Index(value = ["ssn"], unique = true)])
data class MedicalRecords(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int = 0,
    @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "phone_number") val phoneNum: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "first_name") val firstName: String,
    @ColumnInfo(name = "last_name") val lastName: String,
    @ColumnInfo(name = "ssn") val ssn: String,
    @ColumnInfo(name = "address") val address: String,
    @ColumnInfo(name = "postal") val postal: String,
    @ColumnInfo(name = "state") val state: String,
    @ColumnInfo(name = "heartCondtion") val heartCondtion: Boolean = false,
    @ColumnInfo(name = "cancer") val cancer: Boolean = false,
    @ColumnInfo(name = "hiv") val hiv: Boolean = false,
    @ColumnInfo(name = "obesity") val obesity: Boolean = false,
    @ColumnInfo(name = "pregnant") val pregnant: Boolean = false,
    @ColumnInfo(name = "smoker") val smoker: Boolean = false,
    @ColumnInfo(name = "diabetes") val diabetes: Boolean = false,
    @ColumnInfo(name = "transplant") val transplant: Boolean = false,
    @ColumnInfo(name = "immunocompromised") val immunocompromised: Boolean = false,
    @ColumnInfo(name = "birthday") val birthday: String,
    @ColumnInfo(name = "gender")val gender: String,
)
