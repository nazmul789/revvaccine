package com.example.revvaccine.dataBase.BookingsTable

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query



@Dao
interface BookingsDao {

    @Query("SELECT * FROM Bookings")
    fun getBookings(): LiveData<List<Bookings>>

    @Query("SELECT * FROM Bookings")
  suspend fun getBookingsDead(): List<Bookings>

    @Query("SELECT * FROM Bookings WHERE ssn = :ssn")
    fun getBookingsBySSN(ssn: String): LiveData<Bookings>

    @Query("SELECT * FROM Bookings WHERE reference_id = :refId")
    fun getBookingsByReferenceId(refId: String): LiveData<Bookings>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(records: Bookings)

    @Query("DELETE FROM bookings")
    suspend fun deleteAll()

    @Query("DELETE FROM bookings WHERE reference_id = :refId")
    suspend fun deleteByReferenceID(refId: String)


}