package com.example.revvaccine.dataBase.BookingsTable

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "Bookings", indices = [Index(value = ["ssn"], unique = true)])
data class Bookings(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") var id: Int = 0,
    @ColumnInfo(name = "userId") var userId: String,
    @ColumnInfo(name = "ssn") var ssn: String,
    @ColumnInfo(name = "first_name") var firstName: String,
    @ColumnInfo(name = "last_name") var lastName: String,
    @ColumnInfo(name = "date") var date: String,
    @ColumnInfo(name = "time") var time: String,
    @ColumnInfo(name = "reference_id") var referenceId: String,
    @ColumnInfo(name = "medical_records") var medicalRecords: String,
    @ColumnInfo(name = "location_name")var locationName: String,
    @ColumnInfo(name = "location_address") var locationAddress: String?,
    @ColumnInfo(name = "location_city") var locationCity: String,
    @ColumnInfo(name = "location_state") var locationState: String,
    @ColumnInfo(name = "location_postal") var locationPostal: String,
    @ColumnInfo(name = "location_latitude") var locationLatitude: String,
    @ColumnInfo(name = "location_longitude") var locationLongitude: String,
    @ColumnInfo(name = "location_vaccine_type") var locationVaccineType: String,
    @ColumnInfo(name = "appointment_approved") var appointmentApproved: Boolean,
    @ColumnInfo(name = "appointment_canceled") var appointmentCanceled: Boolean = false,
    )

