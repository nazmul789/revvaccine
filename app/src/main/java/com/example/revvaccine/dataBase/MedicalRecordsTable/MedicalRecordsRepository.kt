package com.example.revvaccine.dataBase.MedicalRecordsTable

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecordsDao
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.server.Requests
import timber.log.Timber


class MedicalRecordsRepository(private val medicalRecordsDao: MedicalRecordsDao) {
    val allRecords: LiveData<List<MedicalRecords>> = medicalRecordsDao.getRecords()


    @WorkerThread
    suspend fun allRecordsDead() = medicalRecordsDao.getRecordsDead()

    fun getRecordsBySSN(ssn: String): LiveData<MedicalRecords> =
        medicalRecordsDao.getRecordsBySSN(ssn)

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(medicalRecords: MedicalRecords) {
        medicalRecordsDao.insert(medicalRecords).let {
            Timber.i("SYNC medical Records")
            Requests.recordToBackend(ApplicationModel.firebase.currentUser.uid)
        }
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteAll() {
        medicalRecordsDao.deleteAll()
    }
}