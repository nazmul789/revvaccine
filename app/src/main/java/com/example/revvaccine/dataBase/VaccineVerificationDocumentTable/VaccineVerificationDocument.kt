package com.example.revvaccine.dataBase.VaccineVerificationDocumentTable

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey


@Entity(tableName = "Vaccine_Verification_Document", indices = [Index(value = ["ssn"], unique = true)])
data class VaccineVerificationDocument(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Int = 0,
    @ColumnInfo(name = "userId") val userId: String,
    @ColumnInfo(name = "type") val type: String,
    @ColumnInfo(name = "ssn") val ssn: String,
    @ColumnInfo(name = "photo_id") val photoId: Boolean,
    @ColumnInfo(name = "shot_one") val shotOne: Boolean,
    @ColumnInfo(name = "shot_two") val shotTwo: Boolean,
)
