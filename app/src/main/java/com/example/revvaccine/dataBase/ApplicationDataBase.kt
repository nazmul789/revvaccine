package com.example.revvaccine.dataBase

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import com.example.revvaccine.dataBase.BookingsTable.BookingsDao
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecordsDao
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocument
import com.example.revvaccine.dataBase.VaccineVerificationDocumentTable.VaccineVerificationDocumentDao
import kotlinx.coroutines.CoroutineScope

/**
 * https://developer.android.com/training/data-storage/room
 * https://developer.android.com/codelabs/android-room-with-a-view-kotlin#0
 */
@Database(
    entities = [
        MedicalRecords::class,
        Bookings::class,
        VaccineVerificationDocument::class
    ],
    version = 14
)
abstract class ApplicationDataBase : RoomDatabase() {
    abstract fun medicalRecordsDao(): MedicalRecordsDao
    abstract fun bookingsDao(): BookingsDao
    abstract fun vaccineVerificationDao(): VaccineVerificationDocumentDao

    companion object {
        @Volatile
        private var INSTANCE: ApplicationDataBase? = null

        fun getDatabase(
            context: Context,
            scope: CoroutineScope //Can be used if we need to pre-populate db
        ): ApplicationDataBase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    ApplicationDataBase::class.java,
                    "apps"
                ).fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                instance
            }
        }
    }

}
