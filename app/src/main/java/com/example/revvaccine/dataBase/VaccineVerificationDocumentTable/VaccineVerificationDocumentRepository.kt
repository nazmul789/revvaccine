package com.example.revvaccine.dataBase.VaccineVerificationDocumentTable

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.revvaccine.dataBase.BookingsTable.Bookings
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.server.Requests
import timber.log.Timber

class VaccineVerificationDocumentRepository(private val dao: VaccineVerificationDocumentDao) {

    @WorkerThread
    suspend fun getBookingsAllDead(): List<VaccineVerificationDocument> = dao.getVerificationDead()

    fun getBookingsAll(): LiveData<List<VaccineVerificationDocument>> = dao.getVerification()
    fun getVerificationBySSN(ssn: String): LiveData<VaccineVerificationDocument> = dao.getVerificationBySSN(ssn)

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(doc: VaccineVerificationDocument) {
        dao.insert(doc).let {
            Timber.i("SYNC deleteBySsn")
            Requests.certifiateToBackend(ApplicationModel.firebase.currentUser.uid)
        }
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteBySsn(id: String) {
        dao.deleteBySsn(id).let {
            Timber.i("SYNC deleteBySsn ")
            Requests.certifiateToBackend(ApplicationModel.firebase.currentUser.uid)
        }
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteAll() {
        dao.deleteAll()
    }





}