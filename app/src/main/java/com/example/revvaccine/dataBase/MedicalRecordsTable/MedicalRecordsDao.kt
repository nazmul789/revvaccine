package com.example.revvaccine.dataBase.MedicalRecordsTable

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords


/**
 * https://developer.android.com/reference/androidx/room/Query
 *
 */

@Dao
interface MedicalRecordsDao {

    @Query("SELECT * from medical_records_information")
    fun getRecords(): LiveData<List<MedicalRecords>>

    @Query("SELECT * from medical_records_information")
   suspend fun getRecordsDead(): List<MedicalRecords>

    @Query("SELECT * FROM medical_records_information WHERE phone_number = :phoneNum")
    fun getRecordsByPhoneNum(phoneNum: String): LiveData<List<MedicalRecords>>

    @Query("SELECT * FROM medical_records_information WHERE ssn = :ssn")
    fun getRecordsBySSN(ssn: String): LiveData<MedicalRecords>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(records: MedicalRecords)

    @Query("DELETE FROM medical_records_information")
    suspend fun deleteAll()

}