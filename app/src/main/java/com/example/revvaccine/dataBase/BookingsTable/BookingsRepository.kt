package com.example.revvaccine.dataBase.BookingsTable

import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import com.example.revvaccine.dataBase.MedicalRecordsTable.MedicalRecords
import com.example.revvaccine.models.ApplicationModel
import com.example.revvaccine.server.Requests
import timber.log.Timber

class BookingsRepository(private val bookingsDao: BookingsDao) {
//
//    private val bookingsAll: LiveData<List<Bookings>> =
//    private val bookingsBySSN: LiveData<Bookings>?= null
//    private val bookingsByReference: LiveData<Bookings>?= null


    @WorkerThread
    suspend fun getBookingsAllDead(): List<Bookings> = bookingsDao.getBookingsDead()

    fun getBookingsAll(): LiveData<List<Bookings>> = bookingsDao.getBookings()
    fun getBookingsBySSN(ssn: String): LiveData<Bookings> = bookingsDao.getBookingsBySSN(ssn)
    fun getBookingsByReference(refId: String): LiveData<Bookings> =
        bookingsDao.getBookingsByReferenceId(refId)


    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun insert(bookings: Bookings) {
        bookingsDao.insert(bookings).let {
            Timber.i("SYNC insert book")
            Requests.bookingToBackend(ApplicationModel.firebase.currentUser.uid)

        }
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteByRef(refId: String) {
        bookingsDao.deleteByReferenceID(refId).let {
            Requests.bookingToBackend(ApplicationModel.firebase.currentUser.uid)
        }
    }

    @Suppress("RedundantSuspendModifier")
    @WorkerThread
    suspend fun deleteAll() {
        bookingsDao.deleteAll()
    }
}