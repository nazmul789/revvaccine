# RevVaccine

## What is the purpose of the app?

- RevVaccine’s purpose is to bring people and vaccines together. Anyone who is looking to get a vaccine can go on the app type in their location and book a nearby vaccine appointment.

### Why is this Important?

- Currently there are over 2.8 million people who have died from Corona Virus.
- Multiple cities have restricted people from going out making smaller businesses go broke and file bankruptcy
- Multitudes of people have lost their jobs due to these restrictions directly due to the corona virus
- Quality of life has been affected from the coronavirus
  - People are not allowed to congregate
  - Families are not allowed to meet for holidays or in general
  - Entertainment such as Concerts, Sports, Pools, etc. are all closed.
  - Millions lost their jobs

### How Does the app achieve this goal?

- The team utilizes google maps, along with a clean and easy to use user interface.

### Conclusion

- There are a lot of people who are suffering from the effects of the corona virus, either directly or indirectly. Our goal is to get as many people as possible who want to get vaccinated, vaccinated. This will reduce the number of casualties and infections allowing people to return to their old lives.

## Technologies Used

- Android App
  - Firebase - version 2.0
  - Android SDK
  - Google Map API - version 2.0
- Backend [(Click for repository)](https://github.com/HossainNazmul/RevVaccineBackEndKtor)
  - Ktor- version 2.0
  - Heroku- version 2.0
  - MongoDB - version 2.0
- DevOps
  - Git
  - GitHub
  - Trello

## Features

List of features ready and TODOs for future development

- Register/Login using your email, Gmail, or phone number
- Search vaccine location manually or using your location
- View locations with appointments in list mode or visually in Google Maps
- Book appointments
- You can view, edit, delete, or share your bookings to your calendar
- You can send your medical information
- You can upload your vaccine certificate for verification
- You can share/save your certificate
- Offline mode - Local database will synchronize with backend
- FAQ, Resource, Privacy, Feedback

## Usage

![alt text](https://i.imgur.com/7TCsPxv.png)
![alt text](https://i.imgur.com/Ke592Ij.png)
![alt text](https://i.imgur.com/Re5sG6z.png)
![alt text](https://i.imgur.com/QMnRZYy.png)

## Contributors - version 2.0

- Seth Hunter
- Nazmul Hossain
- Alexander Valdez
- Gloria Lavenberg

## Contributors - version 1.0

> End date: April 7th

- Ankit Patel
- Seth Hunter
- Nazmul Hossain
- Alexander Valdez
- Gloria Lavenberg
- Adam Savoia

## Credit

Location Data provided by vaccinespotter.org API
